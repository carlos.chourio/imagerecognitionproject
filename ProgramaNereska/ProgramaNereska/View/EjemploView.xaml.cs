﻿using ProgramaNereska.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ProgramaNereska.View
{
    /// <summary>
    /// Lógica de interacción para Ejemplo.xaml
    /// </summary>
    public partial class EjemploView : UserControl
    {
        public EjemploView()
        {
            InitializeComponent();
        }

        private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            //((EjemploViewModel)DataContext).AnalizarImagen();
        }

        private void Slider_ValueChanged_1(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            //((EjemploViewModel)DataContext).AnalizarImagen();
        }
    }
}
