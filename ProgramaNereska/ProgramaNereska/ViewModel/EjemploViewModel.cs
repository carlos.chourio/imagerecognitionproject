﻿using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using Microsoft.Win32;
using Prism.Commands;
using ProgramaNereska.Algoritmo;
using ProgramaNereska.Lib;
using ProgramaNereska.Repositorio;
using ProyectoNereska.Modelo;
using System;
using System.Drawing;
using System.IO;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace ProgramaNereska.ViewModel
{
    public class EjemploViewModel : ObservableObject
    {
        #region Variables Privadas
        private float indiceDeDispersividad;
        private float indiceDeContaminacion;
        private float degradacionPonderada;
        private string rutaImagen;
        private readonly RepositorioMuestraAceite repositorioMuestraAceite;
        #endregion

        #region Propiedades
        public float IndiceDeDispersividad
        {
            get { return indiceDeDispersividad; }
            set
            {
                if (indiceDeDispersividad != value)
                {
                    indiceDeDispersividad = value;
                    RaisePropertyChanged();
                }
            }
        }
        public float IndiceDeContaminacion
        {
            get { return indiceDeContaminacion; }
            set
            {
                if (indiceDeContaminacion != value)
                {
                    indiceDeContaminacion = value;
                    RaisePropertyChanged();
                }
            }
        }
        public float DegradacionPonderada
        {
            get { return degradacionPonderada; }
            set
            {
                if (degradacionPonderada != value)
                {
                    degradacionPonderada = value;
                    RaisePropertyChanged();
                }
            }
        }
        public double BarraUno { get; set; }
        public double BarraDos { get; set; }
        public string RutaImagen
        {
            get { return rutaImagen; }
            set
            {
                if (rutaImagen != value)
                {
                    rutaImagen = value;
                    RaisePropertyChanged();
                }
            }
        }
        public Bitmap ImagenAnalizada { get; set; } 
        public ICommand CargarImagenCommand { get; set; }
        public ICommand AnalizarImagenCommand { get; set; }
        public ICommand CargarBaseDeDatosCommand { get; set; }
        #endregion

        public EjemploViewModel(RepositorioMuestraAceite repositorio)
        {
            InicializarCommands();
            CrearDirectorioDeTrabajo();
            repositorioMuestraAceite = repositorio;
        }

        public void CrearDirectorioDeTrabajo()
        {
            //crear carpeta para almacenar imagenes
            string ruta = $"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\\ProgramaNereska\\";
            Directory.CreateDirectory(ruta);
        }

        private void InicializarCommands()
        {
            //enlazar el boton con el metodo
            CargarImagenCommand = new DelegateCommand(CargarImagen);
            AnalizarImagenCommand = new DelegateCommand(AnalizarImagen, () => RutaImagen != null);
            CargarBaseDeDatosCommand = new DelegateCommand(CargarBaseDeDatos);
        }

        public void CargarImagen()
        {
            string ruta = EncontrarImagen();
            if (!string.IsNullOrEmpty(ruta))
            {
                RutaImagen = ruta;
                ((DelegateCommand)AnalizarImagenCommand).RaiseCanExecuteChanged();
            }
        }

        private Rectangle rectangle;
        private int i = 0;

        public void AnalizarImagen()
        {
            #region ///
            //Detection detectorPorRuta = new Detection(RutaImagen);
            //var circulos = detectorPorRuta.Detect();
            //ImagenAnalizada = detector.OriginalImage.Sobel(1, 0, 31).Laplace(31).Bitmap;
            //ImagenAnalizada = Drawing.DrawCircles(new Image<Bgr, Byte>(ImagenAnalizada), detector2.Circles); 
            #endregion
            Image<Bgr, Byte> imagen = new Image<Bgr, byte>(RutaImagen);
            var imagenTemporal = imagen.Canny(BarraUno, BarraDos).Convert<Gray, Byte>().ThresholdBinary(new Gray(230), new Gray(250));
            if (rectangle != null)
            {
                CvInvoke.cvSetImageROI(imagenTemporal, rectangle);
            }
            //Sobel(1, 0, 31)
            VectorOfVectorOfPoint contornos = new VectorOfVectorOfPoint();
            Mat m = new Mat();
            CvInvoke.FindContours(imagenTemporal, contornos, m, Emgu.CV.CvEnum.RetrType.External, Emgu.CV.CvEnum.ChainApproxMethod.ChainApproxSimple);
            CvInvoke.DrawContours(imagen, contornos, -1, new MCvScalar(255, 254, 3), 2);
            //if (contornos.Size>0)
            //{
            //    rectangle = CvInvoke.BoundingRectangle(contornos[i]);
            //    i++;
            //}
            
            ImagenAnalizada = imagen.Bitmap;
            //for (int i = 0; i < contornos.Size; i++)
            //{
            //    double perimeter = CvInvoke.ArcLength(contornos[i], true);
            //    VectorOfPoint puntos = new VectorOfPoint();
            //    CvInvoke.ApproxPolyDP(contornos[i], puntos, 0.04 * perimeter, true);
            //    CvInvoke.DrawContours(imagen, contornos, i, new MCvScalar(255, 254, 3), 2);
                
            //}
            ImagenAnalizada = imagen.Bitmap;
            #region *
            ////ImagenAnalizada = detector.OriginalImage.Canny(25,70).Bitmap;
            RaisePropertyChanged(nameof(ImagenAnalizada));
            //MuestraAceite muestra = new MuestraAceite()
            //{
            //    DegradacionPonderad = 2.5f,
            //    IndiceDeContaminacion = 1.2f,
            //    IndiceDeDispersividad = 1.01f,
            //    RutaImagen = "C:\\Users\\carlo\\OneDrive\\Documentos\\ProgramaNereska\\DlGaZjU.jpg"
            //};
            //await repositorioMuestraAceite.Add(muestra); 
            #endregion
        }

        public void CargarBaseDeDatos()
        {
            MessageBox.Show("Cargar base de datos");
        }

        private string EncontrarImagen()
        {
            string ruta = string.Empty;
            OpenFileDialog cuadroDialogo = new OpenFileDialog();
            cuadroDialogo.Title = "Seleccione la imagen a analizar";
            cuadroDialogo.Filter = "(*.jpg) | *.jpg";
            cuadroDialogo.InitialDirectory = $"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\\ProgramaNereska\\";
            var result = cuadroDialogo.ShowDialog();
            if (result != null && result == true)
            {
                ruta = cuadroDialogo.FileName;
            }
            return ruta;
        }

    }
}
