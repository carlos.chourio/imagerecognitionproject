﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProgramaNereska.ViewModel
{
    public class MainWindowViewModel
    {
        public EjemploViewModel EjemploViewModel { get; }

        public MainWindowViewModel(EjemploViewModel ejemploViewModel)
        {
            EjemploViewModel = ejemploViewModel;
        }
    }
}
