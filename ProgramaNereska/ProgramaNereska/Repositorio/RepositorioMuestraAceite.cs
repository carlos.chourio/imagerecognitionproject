﻿using ProgramaNereska.DataAccess;
using ProyectoNereska.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProgramaNereska.Repositorio
{
    //Manipulacion de la base de datos
    public class RepositorioMuestraAceite : GenericRepository<MuestraAceite, ProgramaNereskaContext>
    {
        public RepositorioMuestraAceite(ProgramaNereskaContext context) : base(context)
        {
        }
    }
}
