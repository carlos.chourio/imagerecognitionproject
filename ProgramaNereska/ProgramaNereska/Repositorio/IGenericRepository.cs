﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProgramaNereska.Repositorio
{
    public interface IGenericRepository<TEntity> where TEntity : class
    {
        Task Add(TEntity model);
        Task<TEntity> Get(int id);
        Task<IEnumerable<TEntity>> FilterBy(Func<TEntity, bool> predicate);
        Task<TEntity> FindFirstBy(Func<TEntity, bool> predicate);
        Task<IEnumerable<TEntity>> GetAll();
        Task Remove(TEntity model);
        Task Update();
    }
}