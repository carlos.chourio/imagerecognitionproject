﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace ProgramaNereska.Repositorio
{
    public class GenericRepository<TEntity, TContext> : IGenericRepository<TEntity>
        where TContext : DbContext
        where TEntity : class
    {
        protected readonly TContext context;

        public GenericRepository(TContext context)
        {
            this.context = context;
        }

        public virtual async Task Add(TEntity model)
        {
            context.Set<TEntity>().Add(model);
            await context.SaveChangesAsync();
        }

        public virtual async Task<TEntity> Get(int id)
        {
            return await context.Set<TEntity>().FindAsync(id);
        }

        public virtual async Task<IEnumerable<TEntity>> GetAll()
        {
            return await context.Set<TEntity>().ToListAsync();
        }

        public virtual async Task<TEntity> FindFirstBy(Func<TEntity,bool> predicate)
        {
            return await Task.Run(()=> context.Set<TEntity>().First(predicate));
        }

        public virtual async Task<IEnumerable<TEntity>> FilterBy(Func<TEntity,bool> predicate)
        {
            return await Task.Run(()=> context.Set<TEntity>().Where(predicate).ToList());
        }

        public virtual async Task Update()
        {
            await context.SaveChangesAsync();
        }

        public virtual async Task Remove(TEntity model)
        {
            context.Set<TEntity>().Remove(model);
            await context.SaveChangesAsync();
        }
    }
}
