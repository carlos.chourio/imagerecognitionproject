﻿using Autofac;
using ProgramaNereska.DataAccess;
using ProgramaNereska.Repositorio;
using ProgramaNereska.ViewModel;

namespace ProgramaNereska.Startup
{
    public class Bootstrapper
    {
        public IContainer Bootstrap()
        {
            ContainerBuilder builder = new ContainerBuilder();
            builder.RegisterType<MainWindowViewModel>().AsSelf();
            builder.RegisterType<EjemploViewModel>().AsSelf();
            builder.RegisterType<MainWindow>().AsSelf();
            builder.RegisterType<ProgramaNereskaContext>().AsSelf();
            builder.RegisterType<RepositorioMuestraAceite>().AsSelf();
            return builder.Build();
        }
    }
}
