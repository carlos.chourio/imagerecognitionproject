﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace ProgramaNereska.Lib
{
    /// <summary>
    /// Clase que notifica los cambios de propiedad
    /// </summary>
    public class ObservableObject : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public void RaisePropertyChanged([CallerMemberName]string propertyName=null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
