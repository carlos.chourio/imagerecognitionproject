﻿using ProyectoNereska.Modelo;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace ProgramaNereska.DataAccess
{
    public class ProgramaNereskaContext : DbContext
    {
        public DbSet<MuestraAceite> TablaMuestraAceite { get; set; }

        public ProgramaNereskaContext() : base("ConexionNere")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}
