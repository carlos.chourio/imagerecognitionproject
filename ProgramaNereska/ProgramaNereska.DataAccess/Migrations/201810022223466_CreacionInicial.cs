namespace ProgramaNereska.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreacionInicial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MuestraAceite",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IndiceDeDispersividad = c.Single(nullable: false),
                        IndiceDeContaminacion = c.Single(nullable: false),
                        DegradacionPonderad = c.Single(nullable: false),
                        RutaImagen = c.String(),
                    })
                .PrimaryKey(t => t.Id);
        }
        
        public override void Down()
        {
            DropTable("dbo.MuestraAceite");
        }
    }
}
