﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoNereska.Modelo
{
    public class Circulo
    {
        public Punto Centro { get; set; }
        public float Radio { get; set; }
        public double Area { get; set; }
    }
    public class Punto
    {
        public float X { get; set; }
        public float Y { get; set; }
    }
}
