﻿namespace ProyectoNereska.Modelo
{
    //Clase utilizada para la tabla de la base de datos
    public class MuestraAceite
    {
        public int Id { get; set; }
        public float IndiceDeDispersividad { get; set; }
        public float IndiceDeContaminacion { get; set; }
        public float DegradacionPonderad { get; set; }
        public string RutaImagen { get; set; }
    }
}