﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using ProyectoNereska.Modelo;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace ProgramaNereska.Algoritmo
{
    public class Detection
    {
        public Image<Bgr, Byte> OriginalImage { get; private set; }
        public UMat GrayScaleImage { get; private set; }
        public IEnumerable<CircleF> Circles { get; private set; }

        public Detection(string imgSource)
        {
            LoadImageFromSource(imgSource);
        }

        public Detection(Bitmap imgSource)
        {
            LoadImageFromSource(imgSource);
        }

        public IEnumerable<Circulo> Detect()
        {
            ConvertImageToGrayScale();
            RemoveNoise();
            Circles = GetCircles();
            IList<Circulo> circulos = ConvertCirclesFToCirculos(Circles);
            return circulos;
        }

        private IList<Circulo> ConvertCirclesFToCirculos(IEnumerable<CircleF> circles)
        {
            IList<Circulo> circulos = new List<Circulo>();
            foreach (var circle in circles)
            {
                circulos.Add(new Circulo
                {
                    Area = circle.Area,
                    Radio = circle.Radius,
                    Centro = new Punto()
                    {
                        X = circle.Center.X,
                        Y = circle.Center.Y
                    }
                });
            }
            return circulos;
        }

        private void LoadImageFromSource(string imgSource)
        {
            OriginalImage = new Image<Bgr, byte>(imgSource)
               .Resize(400, 400, Emgu.CV.CvEnum.Inter.Linear, true);
        }

        private void LoadImageFromSource(Bitmap imgSource)
        {
            OriginalImage = new Image<Bgr, byte>(imgSource)
               .Resize(400, 400, Emgu.CV.CvEnum.Inter.Linear, true);
        }

        private void ConvertImageToGrayScale()
        {
            GrayScaleImage = new UMat();
            CvInvoke.CvtColor(OriginalImage, GrayScaleImage, ColorConversion.Bgr2Gray);
        }

        private void RemoveNoise()
        {
            UMat pyrDown = new UMat();
            CvInvoke.PyrDown(GrayScaleImage, pyrDown);
            CvInvoke.PyrUp(pyrDown, GrayScaleImage);
        }

        private IEnumerable<CircleF> GetCircles()
        {
            double cannyThreshold = 180.0;
            double circleAccumulatorThreshold = 120;
            CircleF[] circles = CvInvoke.HoughCircles(GrayScaleImage, HoughType.Gradient, 2.0, 20.0, cannyThreshold, circleAccumulatorThreshold, 5);
            return circles;
        }
    }
}
