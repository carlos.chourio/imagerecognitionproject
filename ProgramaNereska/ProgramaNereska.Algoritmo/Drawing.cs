﻿using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace ProgramaNereska.Algoritmo
{
    public static class Drawing
    {
        private static Random random = new Random();

        public static Bitmap DrawCircles(Image<Bgr, Byte> image, IEnumerable<CircleF> circles)
        {
            Image<Bgr, Byte> circleImage = image.CopyBlank();
            foreach (CircleF circle in circles)
                circleImage.Draw(circle, new Bgr(Color.FromArgb(random.Next())), 2);
            return circleImage.Bitmap;
        }
    }
}
